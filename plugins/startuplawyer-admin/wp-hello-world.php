<?php
/**
 * An example plugin for a membership course walking readers through how to work with GitHub.
 *
 * @link              https://github.com/tommcfarlin/wp-hello-world/
 * @since             1.0.0
 * @package           WPHW
 *
 * @wordpress-plugin
 * Plugin Name:       Startup Layer Admin
 * Plugin URI:        https://github.com/tommcfarlin/wp-hello-world/
 * Description:       An example plugin for a membership course walking readers through how to work with GitHub.
 * Version:           1.0.0
 * Author:            Raheel Shehzad
 * Author URI:        https://tommcfarlin.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


//Admin CSS Override
function sl_admin_style(){
    wp_register_style( 'sp_admin_css', plugin_dir_url( __FILE__ ).'assets/style.css', false, '1.0.0' );
    wp_enqueue_style( 'sl_admin_style' );
}
add_action('admin_enqueue_scripts', 'sl_admin_style');
add_action('admin_menu', 'my_menu_pages_admin');
// add_action('admin_menu', 'my_menu_pages1');
function my_menu_pages_admin(){
    add_menu_page('StartupLawyer', 'Startup Lawyer', 'manage_options', 'startuplawyer', 'dashboard','dashicons-schedule', 4);
    // add_submenu_page('accounting', 'All Transections', 'All Transections', 'manage_options', 'accounting?type=all', 'all_book');
    // add_submenu_page('accounting', 'Add Transection', 'Add Transection', 'manage_options','accounting?type=add', 'add_tran');
    // add_submenu_page('accounting', 'All Incommings', 'All Incommings', 'manage_options', 'accounting?type=in', 'my_admin_page_contents_aacc');
    // add_submenu_page('accounting', 'All Outgoings', 'All Outgoings', 'manage_options', 'accounting?type=go', 'my_admin_page_contents_aacc');
}
function dashboard()
{
	$file = "templates/index.php";
	ob_start();
include $file;
$output = ob_get_contents();
ob_end_clean();
echo $output;
}
function get_st_temp()
{
    $page = '';
    if(isset($_REQUEST['temp']))
    {
        $page = $_REQUEST['temp'];
    }
    else
    {
        $page = "dash";
    }
    return $page;
}
add_action('admin_menu', 'test_plugin_setup_menu');

function test_plugin_setup_menu(){
    add_menu_page( 'Test Plugin Page', 'Test Plugin', 'manage_options', 'test-plugin', 'test_init' );
}

function test_init(){
    wp_enqueue_script( 'chart-js', '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js' );
    echo "<h1>Hello World!</h1>";
    ?>
    <canvas id="bar-chart" width="800" height="450"></canvas>
    <script>
        window.onload = function(){
            new Chart(document.getElementById("bar-chart"), {
                type: 'bar',
                data: {
                  labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
                  datasets: [
                    {
                      label: "Population (millions)",
                      backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                      data: [2478,5267,734,784,433]
                    }
                  ]
                },
                options: {
                  legend: { display: false },
                  title: {
                    display: true,
                    text: 'Predicted world population (millions) in 2050'
                  }
                }
            });
        };
    </script>
    <?php
}