<?php
include "config.php";
?>
<style type="text/css">
.top-a{
        color: #20882c;
    border:1px solid #20882c;
    background: #f3f5f6;
    text-decoration: none;
    font-size:15px;
    padding: 5px 10px !important;
    border-radius: 5px;
}
.sidenav {
    width: 243px;
    position: relative;
    z-index: 1;
    top: 6px;
    left: 4px;
    background: #eee;
    overflow-x: hidden;
    padding: 8px 0;
}

.sidenav a {
    padding: 13px 12px 12px 14px;
    text-decoration: none;
    font-size: 15px;
    color: #716b6b;
    display: block;
    border-bottom: 1px solid #d2d2d1;
    margin: 5px;
    border-radius: 2px;
}
.sidenav a:hover , .sidenav .active {
    color: #000000;
    background: white;
    margin-right: 0;
}
.main {
  font-size: 28px; /* Increased text to enable scrolling */
  padding: 17px 30px;
  width: 100%;
  overflow-y: scroll;
}
header {
    background: #eeeeee;
    margin-right: 25px;
    position: relative;
    padding: 14px;
    display: flex;
    border: 1px solid #d2d2d1;
    border-bottom: none;
    margin-top: 20px;
}
footer {
    background: #ffffff;
    margin-right: 25px;
    position: relative;
    padding: 20px;
    display: flex;
    border: 1px solid #d2d2d1;
    margin-top: 8px;
}
.Statcis_div {
    float: left;
    width: 200px;
    height: 136px;
    border: 2px solid #eeeeee;
    padding: 10px 12px 11px 9px;
}
.Statcis_div .heading {
    font-size: 23px;
    color: #827e7e;
    margin-bottom: 15px;
}
.statics_btn a {
    font-size: 10px;
    text-decoration: none;
    color: #000000;
    width: 25px;
    height: 25px;
    border: 1px solid;
}
.dropbtn {
  background-color: transparent;
    color: #000;
    padding: 8px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 108px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}

.dropdown:hover .dropbtn {
  background-color: none;
}
@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>
<header>
	<div class="logo"><img src="<?=$plugin_assets;?>images/law.png" style="width: 149px;"></div>
</header>
<section style="display: flex;background: white;margin-right: 25px;height: 575px;border: 1px solid #d2d2d1;border-bottom: none;">
	<div class="sidenav">
		<?php
		foreach ($menu as $key => $value) {
			?>
			<a href="<?= $surl; ?>&temp=<?= $value['type'] ?>" class="<?= ($value['active'])?'active':''; ?>"><?= $value['title']; ?></a>

			<?php
		}
		?>
	</div>
	<div class="main" style="">
		<?php
		$file= get_st_temp();
		$ifile = 'pages/'.$file.'.php';
		include $ifile;
		?>
	</div>

</section>
<footer>Footer</footer>