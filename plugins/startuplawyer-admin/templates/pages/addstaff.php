<h4>Add Staff (Add Admins)</h4>

<form>
    <table class="form-table" role="presentation">
    	<tbody>
    		<tr>
    			<th scope="row">
    				<label for="blogname">User Name</label>
    			</th>
    			<td>
    				<input name="" type="text" id="blogname" value="" class="regular-text">
    			</td>
    		</tr>
    		<tr>
    			<th scope="row">
    				<label for="blogname1">Name</label>
    			</th>
    			<td>
    				<input name="" type="text" id="blogname1" value="" class="regular-text">
    			</td>
    		</tr>
    		<tr>
    			<th scope="row">
    				<label for="blogname2">Email</label>
    			</th>
    			<td>
    				<input name="" type="email" id="blogname2" value="" class="regular-text">
    			</td>
    		</tr>
    		<tr>
    			<th scope="row">
    				<label for="blogdescription">Password</label>
    			</th>
    			<td>
    				<input name="blogdescription" type="password" id="blogdescription" class="regular-text">
    				<p class="description" id="tagline-description">In a few words, explain what this site is about.</p>
    			</td>
    		</tr>
    	</tbody>
    </table>
    <p class="submit">
        <input type="submit" name="" id="submit" class="button button-primary" value="Save Changes">
    </p>
</form>