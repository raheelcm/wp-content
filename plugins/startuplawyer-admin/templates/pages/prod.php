<h4>Products</h4>

<form>
    <p class="search-box" style="margin-bottom: 20px;">
    	<label class="screen-reader-text" for="post-search-input">Search products:</label>
    	<input type="search" id="post-search-input" name="s" value="">
    	<input type="submit" id="search-submit" class="button" value="Search products">
    </p>
</form>


<table class="wp-list-table widefat fixed striped posts">
	<thead>
		<tr>
			<td id="cb" class="manage-column column-cb check-column">
				<label class="screen-reader-text" for="cb-select-all-1">Select All</label>
				<input id="cb-select-all-1" type="checkbox">
			</td>
			<th scope="col" id="thumb" class="manage-column column-thumb"><span class="wc-image tips">Image</span></th>
			<th scope="col" id="name" class="manage-column column-name column-primary sortable desc"><a href=""><span>Name</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" id="sku" class="manage-column column-sku sortable desc"><a href=""><span>SKU</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" id="price" class="manage-column column-price sortable desc"><a href=""><span>Price</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" id="product_cat" class="manage-column column-product_cat">Categories</th>
			<th scope="col" id="date" class="manage-column column-date sortable asc"><a href=""><span>Date</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" id="author" class="manage-column column-author">Author</th>
			<th scope="col" id="visibility" class="manage-column column-visibility">Manage</th>
		</tr>
	</thead>
	<tbody id="the-list">
		<?php
		$args = array(
    'post_type'=> 'product',
    // 'areas'    => 'painting',
    'order'    => 'ASC',
    'posts_per_page'   => -1,
);              

$the_query = new WP_Query( $args );
foreach ($the_query->posts as $key => $value) {
    	$pid = $value->ID;
    	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $pid ), 'single-post-thumbnail' );
    	$image = $image[0];
    	$product = wc_get_product( $pid );


		?>
		<tr id="post-2026" class="iedit author-other level-0 post-2026 type-product status-publish has-post-thumbnail hentry product_cat-webinar">
			<th scope="row" class="check-column">
				<label class="screen-reader-text" for="cb-select-2026"> Select Strategizing for Corona </label>
				<input id="cb-select-2026" type="checkbox" name="post[]" value="2026">
				<div class="locked-indicator"> <span class="locked-indicator-icon" aria-hidden="true"></span> <span class="screen-reader-text">
				<?= get_the_title($pid); ?></span> </div>
			</th>
			<td class="thumb column-thumb" data-colname="Image">
				<a href=""><img width="70" height="70" src="<?= $image ?>" class="attachment-thumbnail size-thumbnail" alt="" srcset="<?= $image ?>"></a>
			</td>
			<td class="name column-name has-row-actions column-primary" data-colname="Name"><strong><a class="row-title" href="http://startuplawyer.strokedev.ml/wp-admin/post.php?post=2026&amp;action=edit"><?= get_the_title($pid); ?></a></strong>
				<div class="hidden" id="inline_2026">
					<div class="post_title"><?= get_the_title($pid); ?></div>
					<div class="post_author">24</div>
					<div class="comment_status">open</div>
					<div class="ping_status">closed</div>
					<div class="_status">publish</div>
					<div class="jj">08</div>
					<div class="mm">09</div>
					<div class="aa">2020</div>
					<div class="hh">12</div>
					<div class="mn">53</div>
					<div class="ss">26</div>
					<div class="post_password"></div>
					<div class="page_template">default</div>
					<div class="post_category" id="product_cat_2026">19</div>
					<div class="tags_input" id="product_tag_2026"></div>
					<div class="tags_input" id="pa_date-and-time_2026"></div>
					<div class="sticky"></div>
				</div>
				<div class="hidden" id="woocommerce_inline_2026">
					<div class="menu_order">0</div>
					<div class="sku"></div>
					<div class="regular_price"><?= $product->get_price();?></div>
					<div class="sale_price"></div>
					<div class="weight"></div>
					<div class="length"></div>
					<div class="width"></div>
					<div class="height"></div>
					<div class="shipping_class"></div>
					<div class="visibility">visible</div>
					<div class="stock_status">instock</div>
					<div class="stock"></div>
					<div class="manage_stock">no</div>
					<div class="featured">no</div>
					<div class="product_type">simple</div>
					<div class="product_is_virtual">yes</div>
					<div class="tax_status">taxable</div>
					<div class="tax_class"></div>
					<div class="backorders">no</div>
					<div class="low_stock_amount"></div>
				</div>
				<div class="row-actions"><span class="id">ID: 2026 | </span><span class="edit"><a href="" aria-label="Edit “Strategizing for Corona”">Edit</a> | </span><span class="inline hide-if-no-js"><button type="button" class="button-link editinline" aria-label="Quick edit “Strategizing for Corona” inline" aria-expanded="false">Quick&nbsp;Edit</button> | </span><span class="trash"><a href="" class="submitdelete" aria-label="Move “Strategizing for Corona” to the Trash">Trash</a> | </span><span class="view"><a href="" rel="bookmark" aria-label="View “Strategizing for Corona”">View</a> | </span><span class="duplicate"><a href="" aria-label="Make a duplicate from this product" rel="permalink">Duplicate</a></span></div>
				<button type="button" class="toggle-row"><span class="screen-reader-text">Show more details</span></button>
			</td>
			<td class="sku column-sku" data-colname="SKU"><span class="na">–</span></td>
			<td class="price column-price" data-colname="Price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span><?= $product->get_price();?></span>
			</td>
			<td class="product_cat column-product_cat" data-colname="Categories"><a href=""><?php
			$terms = get_the_terms($pid, 'product_cat');
	       foreach ($terms as $term) {

	         $product_cat = $term->name;
	            echo $product_cat;
	              break;
	          }
			?></a></td>
			<td class="date column-date" data-colname="Date"><?php
			ucfirst($value->post_status);
			?>
				<br><span title="2020/09/08 12:53:26 pm"><?php
				$date = $value->post_date;
				echo date("d-m-Y", strtotime($date));
				?></span></td>
			<td class="author column-author" data-colname="Author"><a href=""><?php
				$author = $value->post_author;
				echo get_the_author_meta('display_name', $author);
			?></a></td>
			<td class="visibility column-visibility" data-colname="Manage"><a href="">Add Booking</a></td>
		</tr>
		<?php
		}
		?>
	</tbody>
	<tfoot>
		<tr>
			<td class="manage-column column-cb check-column">
				<label class="screen-reader-text" for="cb-select-all-2">Select All</label>
				<input id="cb-select-all-2" type="checkbox">
			</td>
			<th scope="col" class="manage-column column-thumb"><span class="wc-image tips">Image</span></th>
			<th scope="col" class="manage-column column-name column-primary sortable desc"><a href=""><span>Name</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" class="manage-column column-sku sortable desc"><a href=""><span>SKU</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" class="manage-column column-price sortable desc"><a href=""><span>Price</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" class="manage-column column-product_cat">Categories</th>
			<th scope="col" class="manage-column column-date sortable asc"><a href=""><span>Date</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" class="manage-column column-author">Author</th>
			<th scope="col" class="manage-column column-visibility">Manage</th>
		</tr>
	</tfoot>
</table>