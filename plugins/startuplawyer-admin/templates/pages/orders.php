<h4>Orders</h4>

<table class="wp-list-table widefat fixed striped users">
	<thead>
		<tr>
			<td id="cb" class="manage-column column-cb check-column">
				<label class="screen-reader-text" for="cb-select-all-1">Select All</label>
				<input id="cb-select-all-1" type="checkbox">
			</td>
			<th scope="col" id="username" class="manage-column column-username column-primary sortable desc"><a href=""><span>Order ID</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" id="name" class="manage-column column-name">Date Ordered</th>
			<th scope="col" id="name" class="manage-column column-name">Initial Deadline</th>
			<th scope="col" id="name" class="manage-column column-name">Updated Deadline</th>
			<th scope="col" id="name" class="manage-column column-name">Service Provider ID</th>
			<th scope="col" id="name" class="manage-column column-name">Service Provider</th>
			<th scope="col" id="name" class="manage-column column-name">Client Name</th>
			<th scope="col" id="name" class="manage-column column-name">Product Type</th>
			<th scope="col" id="name" class="manage-column column-name">Payment Mode</th>
			<th scope="col" id="name" class="manage-column column-name">Payment Status</th>
			<th scope="col" id="name" class="manage-column column-name">Order Status</th>
			<th scope="col" id="name" class="manage-column column-name">Tracking ID</th>
			<!-- <th scope="col" id="role" class="manage-column column-role">Action</th> -->
		</tr>
	</thead>
	<tbody id="the-list" data-wp-lists="list:user">
		<?php
		$orders = wc_get_orders( array('numberposts' => -1) );
foreach ($orders as $key => $value) {
    	$pid = $value->get_id();
    	$method = get_post_meta( $pid, '_payment_method', true );
    	$items = $value->get_items();
    	$product_id = 0;
    	$i= 0;
    	foreach ( $items as $k=> $item ) {
    		if($i == 0)
    		{
			    $product_name = $item->get_name();
			    $product_id = $item->get_product_id();
			    $product_variation_id = $item->get_variation_id();
			}
			if($product_id)
			{
				$i++;
			}
		}
		$post_author_id = get_post_field( 'post_author', $product_id );
		$recent_author = get_user_by( 'ID', $post_author_id );
		$author_display_name = $recent_author->display_name;
		$customerName = $value->get_billing_first_name().' '.$value->get_billing_last_name();
		$cat = '';
		if($product_id)
		{
			$term_list = wp_get_post_terms($product_id,'product_cat',array('fields'=>'ids'));
			$cat = $cat_id = (int)$term_list[0];
			if( $term = get_term_by( 'id', $cat, 'product_cat' ) ){
			    $cat =  $term->name;
			}

		}



			if($product_id)
			{
		    	?>
				<tr id="user-1">
					<th scope="row" class="check-column">
						<label class="screen-reader-text" for="user_1"></label>
						<input type="checkbox" name="users[]" id="user_1" class="administrator" value="1">
					</th>
					<td class="name column-name" data-colname="Name">Order #<?= $pid; ?></td>
					<td class="name column-name" data-colname="Name"><?=date("d-m-Y", strtotime( $value->date_created)); ?></td>
					<td class="name column-name" data-colname="Name">Deadline</td>
					<td class="name column-name" data-colname="Name">Deadline updated</td>
					<td class="name column-name" data-colname="Name"><?php
					if($post_author_id)
					echo $post_author_id;
					?></td>
					<td class="name column-name" data-colname="Name"><?= $author_display_name; ?></td>
					<td class="name column-name" data-colname="Name"><?= $customerName; ?></td>
					<td class="name column-name" data-colname="Name"><?= $cat; ?></td>
					<td class="name column-name" data-colname="Name"><?= $value->get_payment_method_title(); ?></td>
					<td class="name column-name" data-colname="Name"><?= ($value->get_status() == 'recived' || $value->get_status() == 'completed')?"Paid":"Unpaid"?></td>
					<td class="name column-name" data-colname="Name"><?= $value->get_status() ?></td>
					<td class="name column-name" data-colname="Name">123</td>
					<!-- <td class="role column-role" data-colname="Roles">Action</td> -->
				</tr>
				<?php
			}
}
		?>
	</tbody>
</table>