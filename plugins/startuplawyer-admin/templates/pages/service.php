<h4>Service Provider</h4>
<a href="<?= $aurl.'user-new.php'; ?>" class="top-a">Add Service Provider</a>
<form>
    <p class="search-box" style="margin-bottom: 20px;">
	<label class="screen-reader-text" for="user-search-input">Search Service Provider:</label>
	<input type="search" id="user-search-input" name="s" value="">
	<input type="submit" id="search-submit" class="button" value="Search ">
</p>
</form>

<table class="wp-list-table widefat fixed striped users">
	<thead>
		<tr>
			<td id="cb" class="manage-column column-cb check-column">
				<label class="screen-reader-text" for="cb-select-all-1">Select All</label>
				<input id="cb-select-all-1" type="checkbox">
			</td>
			<th scope="col" id="name" class="manage-column column-name">ID</th>
			<th scope="col" id="username" class="manage-column column-username column-primary sortable desc"><a href=""><span>Username</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" id="name" class="manage-column column-name">Name</th>
			<th scope="col" id="email" class="manage-column column-email sortable desc"><a href=""><span>Email</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" id="email" class="manage-column column-email sortable desc"><a href=""><span>Date</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" id="email" class="manage-column column-email sortable desc"><a href=""><span>Status</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" id="role" class="manage-column column-role">Action</th>
		</tr>
	</thead>
	<tbody id="the-list" data-wp-lists="list:user">
		<?php

$args = array(
    'role'    => 'service_provider',
    'orderby' => 'user_nicename',
    'order'   => 'ASC'
);
$users = get_users( $args );

// echo '<ul>';
foreach ( $users as $user ) {
	$udata = get_userdata( $user->ID );
    $registered = $udata->user_registered;
    $varified = get_user_meta($user->ID, 'varified',true);
    ?>
		<tr id="user-1">
			<th scope="row" class="check-column">
				<label class="screen-reader-text" for="user_1">Select admin</label>
				<input type="checkbox" name="users[]" id="user_1" class="administrator" value="1">
			</th>
			<td><?= $user->ID; ?></td>
			<td class="username column-username has-row-actions column-primary" data-colname="Username"><img alt="" src="<?= get_user_img($user->ID); ?>" height="32" width="32"> <strong><a href="http://startuplawyer.strokedev.ml/wp-admin/profile.php?wp_http_referer=%2Fwp-admin%2Fusers.php"><?= esc_html( $user->display_name ) ?></a></strong>
				<br>
				<div class="row-actions"><span class="edit"><a href="">Edit</a> | </span><span class="view"><a href="http://startuplawyer.strokedev.ml/author/admin/" aria-label="View posts by Raheel">View</a></span></div>
				<button type="button" class="toggle-row"><span class="screen-reader-text">Show more details</span></button>
			</td>
			<td class="name column-name" data-colname="Name"><?= esc_html( $user->display_name ) ?></td>
			<td class="email column-email" data-colname="Email"><a href="mailto:raheelshehzad188@gmail.com"><?= $user->user_email; ?></a></td>
			<td class="email column-email" data-colname="Email"><?= date( "d M Y", strtotime( $registered )); ?></td>
			<td class="email column-email" data-colname="Email">
				<?php
					if($varified)
					{
						?><span class="badge badge-active" style="color: white;background-color: #28a745;padding: 3px;">Active</span><?php
					}
					else
					{
						?><span class="badge badge-pending" style="background-color: #ffc107;padding: 3px;">Pending</span><?php
					}
				?>
			</td>
			<td class="role column-role" data-colname="Roles">
				<div class="dropdown">
				  <button class="dropbtn">.....</button>
				  <div class="dropdown-content">
				  <a href="<?= $aurl; ?>user-edit.php?user_id=<?= $user->ID?>">Edit</a>
				  <a href="#">Login as Service provider</a>
				  </div>
				</div>

			</td>
		</tr>
		    <?php
}
// echo '</ul>';

?>
	</tbody>
	<tfoot>
		<tr>
			<td class="manage-column column-cb check-column">
				<label class="screen-reader-text" for="cb-select-all-2">Select All</label>
				<input id="cb-select-all-2" type="checkbox">
			</td>
			<th scope="col" class="manage-column column-username column-primary sortable desc"><a href="http://startuplawyer.strokedev.ml/wp-admin/users.php?orderby=login&amp;order=asc"><span>Username</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" class="manage-column column-name">Name</th>
			<th scope="col" class="manage-column column-email sortable desc"><a href="http://startuplawyer.strokedev.ml/wp-admin/users.php?orderby=email&amp;order=asc"><span>Email</span><span class="sorting-indicator"></span></a></th>
			<th scope="col" class="manage-column column-role">Roles</th>
		</tr>
	</tfoot>
</table>