<h4>Dashboard</h4>
<?php
wp_enqueue_script( 'chart-js', '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js' );
    ?>
    <canvas id="bar-chart" style="height: 400px;width: 400px;" width="400" height="400"></canvas>
    <script>
        window.onload = function(){
            new Chart(document.getElementById("bar-chart"), {
            	height:400,
                type: 'bar',
                data: {
                  labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
                  datasets: [
                    {
                      label: "Population (millions)",
                      backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                      data: [2478,6267,734,784,433]
                    }
                  ]
                },
                options: {
                	height: 400,
                	maintainAspectRatio: false,
                  legend: { display: false },
                  title: {
                    display: true,
                    text: 'Predicted world population (millions) in 2050'
                  }
                }
            });
            jQuery(document).ready(function(){
            	jQuery('#bar-chart').css('height',400);
            	jQuery('#bar-chart').css('width',400);
            	jQuery('#bar-chart').css('float','left');
            });
        };
    </script>

    <div class="Statcis_div">
    	<div class="heading">Statics</div> 
    	<div class="statics_btn">
    		<a href="" >OverAll</a>
    		<a href="" >This Week</a>
    		<a href="" >This Month</a>
    		<a href="" >This Year</a>
    	</div>
    </div>
    <!-- <div class="earning">
    	<div class="heading">Earnings</div>
    	<table>
    		<tr>
    			<td><input type="text" name=""></td>
    			<td><input type="text" name=""></td>
    		</tr>
    		<tr>
    			<td><span class="bluetag">TOTAL EARNINGS</span></td>
    			<td><span class="">$ 409.50</span></td>
    		</tr>
    		<tr>
    			<td colspan="2"><p>This is the overall amount of startup lawyer</p></td>
    		</tr>
    		<tr>
    			<td><span class="bluetag">COMMISIONS</span></td>
    			<td><span class="">$ 409.50</span></td>
    		</tr>
    		<tr>
    			<td><span class="bluetag">SUBSCRIPTIONS</span></td>
    			<td><span class="">$ 409.50</span></td>
    		</tr>
    		<tr>
    			<td><span class="bluetag">CHARGES</span></td>
    			<td><span class="">$ 409.50</span></td>
    		</tr>
    	</table>
    </div> -->