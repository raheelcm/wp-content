<?php
$plugin_dir_name = 'startuplawyer-admin';
$plugin_assets = plugin_dir_url('').$plugin_dir_name.'/assets/';
$pcolor= '';
$surl = site_url('/wp-admin/admin.php').'?page=startuplawyer';
$aurl = site_url('/wp-admin/');
$menu = array();
$file = get_st_temp();
$menu[] = array(
"title"=> 'Dashboard',
"type"=> 'dash',
"icon"=> 'Dashboard',
"active"=> 1,
);
$menu[] = array(
"title"=> 'Service provider',
"type"=> 'service',
"icon"=> 'Dashboard',
"active"=> 0,
);
$menu[] = array(
"title"=> 'Products',
"type"=> 'prod',
"icon"=> 'Dashboard',
"active"=> 0,
);
$menu[] = array(
"title"=> 'Add Staff (sub admins)',
"type"=> 'addstaff',
"icon"=> 'Dashboard',
"active"=> 0,
);
$menu[] = array(
"title"=> 'Facilitate',
"type"=> 'facil',
"icon"=> 'Dashboard',
"active"=> 0,
);
$menu[] = array(
"title"=> 'Orders',
"type"=> 'orders',
"icon"=> 'Dashboard',
"active"=> 0,
);
$menu[] = array(
"title"=> 'Commissions',
"type"=> 'commis',
"icon"=> 'Dashboard',
"active"=> 0,
);
$menu[] = array(
"title"=> 'Payments',
"type"=> 'paym',
"icon"=> 'Dashboard',
"active"=> 0,
);
$menu[] = array(
"title"=> 'Massage Configration',
"type"=> 'masg',
"icon"=> 'Dashboard',
"active"=> 0,
);
$menu[] = array(
"title"=> 'Chat bot & FAQs',
"type"=> 'chat',
"icon"=> 'Dashboard',
"active"=> 0,
);
foreach ($menu as $key => $value) {
	if($value['type'] == $file)
	{
		$menu[$key]['active'] = 1;
	}
	else
	{
		$menu[$key]['active'] = 0;
	}
}
?>