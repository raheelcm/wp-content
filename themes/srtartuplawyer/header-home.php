<?php 

$url = get_assets_url();

?>

	<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="Foogra - Discover & Book the best restaurants at the best price">

    <meta name="author" content="Ansonika">

    <title><?php echo get_bloginfo( 'name' ); ?></title>



	<!-- Select2 CDN link -->

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" integrity="sha512-RWhcC19d8A3vE7kpXq6Ze4GcPfGe3DQWuenhXAbcGiZOaqGojLtWwit1eeM9jLGHFv8hnwpX3blJKGjTsf2HxQ==" crossorigin="anonymous" />

<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Favicons-->

    <link rel="shortcut icon" type="image/x-icon" href="<?= site_url() ;?>/wp-content/uploads/2020/11/cebab310d0de566d1a073d52099b683f-png-512x512-1.png">

    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">

    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">

    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">

    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">



    <!-- GOOGLE WEB FONT -->

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js" integrity="sha512-jGR1T3dQerLCSm/IGEGbndPwzszJBlKQ5Br9vuB0Pw2iyxOy+7AK+lJcCC8eaXyz/9du+bkCy4HXxByhxkHf+w==" crossorigin="anonymous"></script>



    <!-- BASE CSS -->

    <link href="<?= $url; ?>css/bootstrap_customized.min.css" rel="stylesheet">

    <link href="<?= $url; ?>css/style.css" rel="stylesheet">




    <!-- SPECIFIC CSS -->

    <link href="<?= $url; ?>css/home.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">



    <!-- YOUR CUSTOM CSS -->

    <link href="<?= $url; ?>css/custom.css" rel="stylesheet">

    <style>

        .cart_img {

            width: 23px;

            margin-top: 8px;

        }

      .top-header ul {

    float: none;

    padding: 38px 89px 0 0;

    width: 100%;

    text-align: right;

    margin: 0;

}


        .top-header ul li {

    display: inline-block;

    color: #000 !important;

    font-weight: bold;

    margin: 0 12px;

}

        .top-header ul li a, .top-header ul li {

          color:#000;

        }

        .top-header ul li a:hover ,.sticky .container .top-header ul li a:hover{

            color:#589442;

        }
        .cdiv {
            /*background: red;*/
            border: 1px solid;
            border-radius: 50px;
            width: 21px;
            padding: 3px;
            text-align: center;
            position: relative;
            top: -40px;
            left: 10px;
        }
        
        .sticky ul li a .cdiv {
            color: white;
        }

        .sticky .container .top-header ul li{

            color:black;

        }
         @media only screen and (max-width: 991px){
            #header_menu {
                display: block !important;
                background-color: #182e49;
                height: 76px !important;
            }
            #header_menu a.open_close {
                top: 25px;
                right: 37px !important;
            }
        }

       

    </style>


</head>



<body>

			

	<header class="header clearfix element_to_stick">

		<div class="container_">
			<div class="top-header" style="background: transparent;">

			    <div class="row" style="background: transparent;">

			        <div class="col-md-6" style="background: transparent;"></div>

			        <div class="col-md-6" style="background: transparent;">

		<?php

		    //include "top-header.php";

    	?>
    				</div>

			    </div>

			</div>

		<div id="logo">

			<a href="<?= get_option('siteurl'); ?>">

				<img src="<?= ot_get_option( 'site_logo', $url.'img/logo2.png' ) ?>" width="110" height="48" alt="" class="logo_normal">

				<img src="<?= ot_get_option( 'sticky_logo', $url.'img/logo2.png' ) ?>" width="110" height="48" alt="" class="logo_sticky">

			</a>

		</div>

		<?php

		    include "nav.php";

		?>

		</div>

	</header>

	<!-- /header -->

	