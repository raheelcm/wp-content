<?php /* Template Name: SignIn page */ 
// die("OJ");
$url = get_assets_url();
?>
<?php get_header(); ?>
<main class="bg_gray pattern">
		
		<div class="container margin_60_40">
		    <div class="row justify-content-center">
		        <div class="col-lg-4">
		        	<div class="sign_up">
		                <div class="head">
		                    <div class="title">
		                    <h3>Sign In</h3>
		                </div>
		                </div>
		                <!-- /head -->
		                <div class="main">
		                    <!--
		                	<a href="#0" class="social_bt facebook">Sign up with Facebook</a>
							<a href="#0" class="social_bt google">Sign up with Google</a>
							
							<div class="divider"><span>Or</span></div>-->
		                	<?php echo do_shortcode('[[wpr-form id]]'); ?>
		                </div>
		            </div>
		            <!-- /box_booking -->
		        </div>
		        <!-- /col -->

		    </div>
		    <!-- /row -->
		</div>
		<!-- /container -->
		
	</main>

<?php get_footer(); ?> 