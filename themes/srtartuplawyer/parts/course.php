<?php

$terms = get_the_terms( $pid, 'product_cat' );
$product = wc_get_product( $pid );
     $post = get_post( $pid );
$recent_author = get_user_by( 'ID',$post->post_author);
$dead_line = get_post_meta($pid,'dead_line',true);
$img = get_the_post_thumbnail_url($pid);
if(!$img)
{
    $img = ot_get_option( 'product_default_image', '' );
}




?>
							<div class="strip">

							    <figure>

							        <img src="<?= $img; ?>" data-src="<?= $img; ?>" class="img-fluid lazy" alt="">

							        <div class="strip_info">

							            <small>

							            	<?php

							            	$terms = get_the_terms( $pid, 'product_cat' );

                                    foreach($terms as $sing)

                                    {

                                        echo $sing->name.'<br>';

                                    }

                                    ?>

							            </small>
							            <?php
                                        if($terms[0]->term_id != 17)
                                        {
                                            ?>
                                            
							                <a  class="pname" style="float:right;background: #000;color: #fff;" href="<?= panel_url('/index/profile'); ?>/<?= $recent_author->user_login; ?>">
							                <?= $recent_author->display_name; ?>
							                </a>
							            
							            <?php
                                        }
							            ?>
							            <a  href="<?= get_permalink($pid); ?>" >

							            <div class="item_title">

							                <h3><?= get_the_title($pid); ?></h3>

							                <small class="d-none">27 Old Gloucester St</small>

							            </div>
							            </a>
							            </div>

							        

							    </figure>

							    <ul>

							         <li><span><?= $product->get_price_html(); ?></span></li>

							        <li>
                                        <?php
                                        if($terms[0]->term_id != 17)
                                        {
                                            ?>
                                            <div class="score"><span>Delivery <em> Time</em></span><strong><?= $dead_line ?> days</strong></div>
                                            <?php
                                        }
                                        ?>

							        </li>

							    </ul>

							</div>