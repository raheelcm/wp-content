<?php
$slug = get_post_field( 'post_name', get_post() );
$img = get_the_post_thumbnail_url($pid);
if(!$img)
{
    $img = ot_get_option( 'product_default_image', '' );
}
?>
<div class="item">
    

						<div class="card" style="width: 18rem;">
						    <div class="webinar_name" ><?= get_the_title($pid); ?></div>

							<a class="itema" href="<?= get_permalink($pid); ?>"><img class="card-img-top" src="<?= $img; ?>" alt="Card image cap"></a>
							<div class="more_info" >
							    <span class="lang"><?php 
							    if(get_post_meta($pid,'lanaguage',true))
							    {
							        echo get_the_title(get_post_meta($pid,'lanaguage',true));
							    }
							    ?></span>
							    <span class="wdate">
							        <?php
							        $title = '';
							        $args = array(
                                        'post_parent' => '7488',
                                        'post_type' => 'wslot',
                                    );
                                    
                                    $posts_array = new WP_Query($args);
                                    foreach($posts_array->posts as $v)
                                    {
                                        $title = $title.$v->post_title.'<br>';
                                    }
                                    echo $title;
							        ?></span>
							    
							    
							</div>
							<?php
							if($slug != 'search-service-provider')
							{
							    ?>

							<div class="card-body" style="text-align: center;">

							    <button class="btn_sale">Register</button>

							</div>
							<?php
							}
							?>

						</div>

					</div>