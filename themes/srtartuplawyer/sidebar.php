<?php 
$url = get_assets_url();?>
<aside id="sidebar">
<div class="widget">
						<div class="widget-title first">
							<h4>Latest Post</h4>
						</div>
						<ul class="comments-list">
							<?php $recent_posts = wp_get_recent_posts(array(
								'numberposts' => 4, // Number of recent posts thumbnails to display
								'post_status' => 'publish' // Show only the published posts
							));
							foreach($recent_posts as $post) : ?>
								<li>
									<div class="alignleft">
										<a href="<?php echo get_permalink($post['ID']) ?>"><img src="<?php echo get_the_post_thumbnail_url($post['ID']) ?>" alt=""></a>
									</div>
									<small>Category - 11.08.2016</small>
									<h3><a href="<?php echo get_permalink($post['ID']) ?>" title=""><?php echo $post['post_title'] ?></a></h3>
								</li>
							<?php endforeach; wp_reset_query(); ?>
						</ul>
					</div>
					<!-- /widget -->
					<div class="widget">
						<div class="widget-title">
							<h4>Categories</h4>
						</div>
						<ul class="cats">
							<li><a href="#">Food <span>(12)</span></a></li>
							<li><a href="#">Places to visit <span>(21)</span></a></li>
							<li><a href="#">New Places <span>(44)</span></a></li>
							<li><a href="#">Suggestions and guides <span>(31)</span></a></li>
						</ul>
					</div>
					<!-- /widget -->
					<div class="widget">
						<div class="widget-title">
							<h4>Popular Tags</h4>
						</div>
						<div class="tags">
							<a href="#">Food</a>
							<a href="#">Bars</a>
							<a href="#">Cooktails</a>
							<a href="#">Shops</a>
							<a href="#">Best Offers</a>
							<a href="#">Transports</a>
							<a href="#">Restaurants</a>
						</div>
					</div>
					<!-- /widget -->
</aside>