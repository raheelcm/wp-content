<h1>
				<?php esc_html_e( 'All Products', 'my-plugin-textdomain' ); ?>
				
			</h1>
			<table class="widefat fixed" cellspacing="0">
    <thead>
        <tr>
            <th id="cb" class="manage-column column-cb " scope="col">Service Provider Name</th> 
            <th id="columnname" class="manage-column column-columnname" scope="col">Booking product ID</th>
            <th id="columnname" class="manage-column column-columnname num" scope="col">Booking product Name</th> 
            <th id="columnname" class="manage-column column-columnname num" scope="col">Image</th> 
            <th id="columnname" class="manage-column column-columnname num" scope="col">Price</th> 
            <th id="columnname" class="manage-column column-columnname num" scope="col">Booking Type</th> 
            <th id="columnname" class="manage-column column-columnname num" scope="col">Status</th> 
            <th id="columnname" class="manage-column column-columnname num" scope="col">Action</th> 
        </tr>
    </thead>
    <tbody>
        <?php
        $args = array(
    'post_type'=> 'bookings_parent',
    'areas'    => 'painting',
    'order'    => 'DESC'
);              

$the_query = new WP_Query( $args );
if($the_query->have_posts() ) : 
    while ( $the_query->have_posts() ) : 
       $the_query->the_post(); 
       $pid = get_the_ID();
       $user_id=get_post_meta($pid,'user_id',true);
       
       $meta = get_post_meta($pid);
       $price  = get_post_meta($pid,'price',true);
    //   $booking_type = get_post_meta($pid,'booking_type',true);
       $booking_type  = get_post_meta($pid,'booking_type',true);
       $recent_author = get_user_by( 'id', $user_id );
    //   var_dump($recent_author);
    $featured_img_url = get_the_post_thumbnail_url($pid,'thumbnail');
    if(empty($featured_img_url ))
     $featured_img_url = 'https://thumbs.dreamstime.com/b/no-image-available-icon-flat-vector-illustration-132483053.jpg';


       ?>
       <tr>
            <td id="cb" class="manage-column column-cb " scope="col"><?php 
echo $author_display_name = $recent_author->display_name;
 ?></td> 
            <td id="columnname" class="manage-column column-columnname" scope="col"><?= 'lawyer'.$user_id.'-'.$pid ?></td>
            <td id="columnname" class="manage-column column-columnname num" scope="col"><?= get_the_title(); ?></td> 
            <td id="columnname" class="manage-column column-columnname num" scope="col"><img width="50" src="<?= $featured_img_url; ?>"/></td> 
            <td id="columnname" class="manage-column column-columnname num" scope="col"><?= $price; ?></td> 
            <td id="columnname" class="manage-column column-columnname num" scope="col"><?= get_the_title($booking_type); ?></td> 
            <td id="columnname" class="manage-column column-columnname num" scope="col">-</td>
            <td id="columnname" class="manage-column column-columnname num" scope="col"><div class="row-actions">
                    <span><a href="?page=my-menu&edit_id=<?= $pid; ?>&type=add">Edit</a> </span>
                </div></td> 
        </tr>
        <?php
    endwhile; 
    wp_reset_postdata(); 
else: 
endif;
?>
        
    </tbody>
    <tfoot>
        <tr>
            <th class="manage-column column-cb check-column" scope="col"></th>
            <th class="manage-column column-columnname" scope="col"></th>
            <th class="manage-column column-columnname num" scope="col"></th>
            <th class="manage-column column-columnname num" scope="col"></th>
            <th class="manage-column column-columnname num" scope="col"></th>
            <th class="manage-column column-columnname num" scope="col"></th>
            <th class="manage-column column-columnname num" scope="col"></th>
            <th class="manage-column column-columnname num" scope="col">
            </th>
    </tr>
    </tfoot>
</table>