<h1>
				<?php esc_html_e( 'All Booking products', 'my-plugin-textdomain' ); ?>
				
			</h1>
			<table class="widefat fixed" cellspacing="0">
    <thead>
    <tr>

            <th id="cb" class="manage-column column-cb " scope="col">Image</th> 
            <th id="columnname" class="manage-column column-columnname" scope="col">Name</th>
            <th id="columnname" class="manage-column column-columnname num" scope="col">Action</th> 

    </tr>
    </thead>

    <tfoot>
    <tr>

            <th class="manage-column column-cb check-column" scope="col"></th>
            <th class="manage-column column-columnname" scope="col"></th>
            <th class="manage-column column-columnname num" scope="col"></th>

    </tr>
    </tfoot>

    <tbody>
        <?php

/*$args = array(
    'role'    => 'service_provider',
    'orderby' => 'user_nicename',
    'order'   => 'ASC'
);
$users = get_users( $args );*/
$products = wc_get_products(array(
    'category' => array('booking'),
));
            

// echo '<ul>';
?>
    <?php
foreach ( $products as $sing ) {
    // print_r($sing);
    $pid = $sing->get_id();
    ?>
    <tr class="alternate">
            <th class="check-column" scope="row">
                <img width="50" src="<?= (!empty(get_the_post_thumbnail_url($pid)))?get_the_post_thumbnail_url($pid):ot_get_option( 'product_default_image', '' ); ?>"/>
            </th>
            <td class="column-columnname"><?= $sing->get_name() ?></td>
            <td class="column-columnname">
                <div class="row-actions">
                    <span><a href="?page=my-menu&product_id=<?= $pid; ?>&type=add">Add</a> </span>
                </div>
            </td>
        </tr>
    <?php
}
// echo '</ul>';

?>
        
    </tbody>
</table>