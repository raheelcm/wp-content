<?php /* Template Name: Signup page */ 
// die("OJ");
$url = get_assets_url();
?>
<?php get_header(); ?>
<main class="bg_gray pattern">
		
		<div class="container margin_60_40">
		    <div class="row justify-content-center">
		        <div class="modal-dialog wpr-login-wrapper">
		        	<div class="modal-content">
		                <div class="modal-heading">
		                    <div class="text-center">
		                    <h3>Sign Up</h3>
		                </div>
		                </div>
		                <!-- /head -->
		                <div class="main">
		                    <!--
		                	<a href="#0" class="social_bt facebook">Sign up with Facebook</a>
							<a href="#0" class="social_bt google">Sign up with Google</a>
							
							<div class="divider"><span>Or</span></div>-->
		                	<?php echo do_shortcode('[wpr-form id="251"]'); ?>
		                </div>
		            </div>
		            <!-- /box_booking -->
		        </div>
		        <!-- /col -->

		    </div>
		    <!-- /row -->
		</div>
		<!-- /container -->
		
	</main>
	<div class="call_section lazy" id="are_youlawyer" data-bg="url('<?= ot_get_option( 'banner_image_3', $url.'img/lowyer2.jpg' ) ?>">

		    <div class="container clearfix">

		        <div class="col-lg-5 col-md-6 float-right wow">

		            <div class="box_1">

		                <h3>Are you a Lawyer, Corporate

Secretary, Business Consultant or Mediator?</h3>

		                <p> Join us to increase your

online visibility.</p>

		                <a href="submit-restaurant.html" class="btn_1">Read more</a>

		            </div>

		        </div>

    		</div>

    	</div>

	

<?php get_footer(); ?> 