<h1>
				<?php esc_html_e( 'All service providers', 'my-plugin-textdomain' ); ?>
				
			</h1>
			<table class="widefat fixed" cellspacing="0">
    <thead>
    <tr>

            <th id="cb" class="manage-column column-cb " scope="col">Image</th> 
            <th id="columnname" class="manage-column column-columnname" scope="col">Name</th>
            <th id="columnname" class="manage-column column-columnname num" scope="col">Credit Balance</th> 
            <th id="columnname" class="manage-column column-columnname num" scope="col">Balance</th> 
            <th id="columnname" class="manage-column column-columnname num" scope="col">Action</th> 

    </tr>
    </thead>

    <tfoot>
    <tr>

            <th class="manage-column column-cb check-column" scope="col"></th>
            <th class="manage-column column-columnname" scope="col"></th>
            <th class="manage-column column-columnname num" scope="col"></th>

    </tr>
    </tfoot>

    <tbody>
        <?php

$args = array(
    'role'    => 'service_provider',
    'orderby' => 'user_nicename',
    'order'   => 'ASC'
);
$users = get_users( $args );

// echo '<ul>';
foreach ( $users as $user ) {
    ?>
    <tr class="alternate">
            <th class="check-column" scope="row"><img src="<?php echo get_user_img( $user->data->ID); ?>" width="50" /></th>
            <td class="column-columnname"><?= esc_html( $user->display_name ) ?></td>
            <td class="column-columnname" style="text-align: center;"><?= get_user_meta($user->ID,'cbalance',true) ?></td>
            <td class="column-columnname" style="text-align: center;"><?= get_user_meta($user->ID,'balance',true) ?></td>
            <td class="column-columnname" style="text-align: center;">
                <div class="row-actions">
                    <span><a href="?page=my-menu&user_id=<?= $user->data->ID; ?>&type=add">Add</a> |</span>
                    <span><a href="?page=my-menu&user_id=<?= $user->data->ID; ?>&type=manage">Manage</a></span>
                </div>
            </td>
        </tr>
    <?php
}
// echo '</ul>';

?>
        
    </tbody>
</table>