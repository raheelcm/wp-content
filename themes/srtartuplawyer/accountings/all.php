<h1>
				<?php esc_html_e( 'All Transactions', 'my-plugin-textdomain' ); ?>
				
			</h1>
			<table class="widefat fixed" cellspacing="0" >
    <thead>
    <tr>

            <th id="cb" class="manage-column column-cb " style="text-align: center;" scope="col">#</th> 
            <th id="cb" class="manage-column column-cb " style="text-align: center;" scope="col">Transaction id</th> 
            <th id="columnname" class="manage-column column-columnname" style="text-align: center;" scope="col">Name</th>
            <th id="cb" class="manage-column column-cb " style="text-align: center;" scope="col">Amount</th> 
            <th id="cb" class="manage-column column-cb " style="text-align: center;" scope="col">Transaction Type</th> 
            <th id="columnname" class="manage-column column-columnname num" style="text-align: center;" scope="col">Action</th> 

    </tr>
    </thead>
    <tbody>
        <tr>
            <td class="manage-column column-cb check-column" style="text-align: center;" scope="col">1</td>
            <td class="manage-column column-columnname" style="text-align: center;" scope="col">00000</td>
            <td class="manage-column column-columnname num" style="text-align: center;" scope="col">Name</td>
            <td class="manage-column column-columnname num" style="text-align: center;" scope="col">$5</td>
            <td class="manage-column column-columnname num" style="text-align: center;" scope="col"><div alt="f346" class="dashicons dashicons-arrow-down-alt"></div></td>
            <td style="text-align: center;"></td>
        </tr>
        <tr>
            <td class="manage-column column-cb check-column" scope="col">1</td>
            <td class="manage-column column-columnname" scope="col">00000</td>
            <td class="manage-column column-columnname num" scope="col">Name</td>
            <td class="manage-column column-columnname num" scope="col">$5</td>
            <td class="manage-column column-columnname num" scope="col"><div alt="f342" class="dashicons dashicons-arrow-up-alt"></div></td>
            <td></td>
        </tr>
    </tbody>

    <tfoot>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    </tfoot>

    <tbody>
        <?php

$args = array(
    'role'    => 'service_provider',
    'orderby' => 'user_nicename',
    'order'   => 'ASC'
);
$users = get_users( $args );

// echo '<ul>';
foreach ( $users as $user ) {
    ?>
    <tr class="alternate">
            <th class="check-column" scope="row"><?php echo get_avatar( $user->data->ID, 50 ); ?></th>
            <td class="column-columnname"><?= esc_html( $user->display_name ) ?></td>
            <td class="column-columnname">
                <div class="row-actions">
                    <span><a href="?page=my-menu&user_id=<?= $user->data->ID; ?>&type=add">Add</a> |</span>
                    <span><a href="?page=my-menu&user_id=<?= $user->data->ID; ?>&type=manage">Manage</a></span>
                </div>
            </td>
        </tr>
    <?php
}
// echo '</ul>';

?>
        
    </tbody>
</table>